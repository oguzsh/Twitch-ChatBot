# Twitch-Bot
Twitch Bot with tmi.js and Node.js.

// Command's
 
!game - Game change with command

!title - Title change with command

!şarkı - Printing the playing song on Spotify

!abone   - Sub Count

!süre - Uptime


# Contributors
Yılmaz Yağız Dokumacı - https://github.com/yagizdo

Selim Yalınkılıç - https://github.com/selimyalinkilic

Oğuzhan İnce - https://github.com/oguzsh
